# Getting Started with Create React App "Greater APP Ever"

## Introduction - Front end
This project uses the ReactJS for implementation purpose. ReactJS was choosen  because of the way it solves the problems we had with Traditional single page application development frame works starting with state management keeping track of the UI and managing the state can be hard and time consuming is one of the most popular and quickly growing single page application development framework.

Used Technologies
1. ReactJS with vscode as IDE
2. Typscript, Bootstrap and Javascript
3. Build and deployed Tool: npm

### Starten der Anwendung
- The application can be startet deployed using the npm command Line as npm start

### Bedienung

- After start the client side using "npm start" on the command line, the browser will automaticaly start with the URL:
http://localhost:3000/
and the app will present a Text field where the end user should enter a valid URL without http:// or https://. For example, www.azetie.net or azeti.net and click on Request Title then the App will display the title of the request page.
API server integration from Spring Boot should be up and running in the background, so your client-side resources could interact with Rest API dynamically to get the desired Data. 

## To-Dos
Unit test

