import './App.css';
import {Route,Switch} from 'react-router-dom';
import { BrowserRouter as Router} from 'react-router-dom'; 
import RequestTitle from './components/RequestTitle.js';
import Header from "./components/Header.js";


function App() {
  return (
  
    <Router>
    <div className="App">
    <Header />
      <hr />
        <Route exact path="/" component={RequestTitle}/>   
    </div>
    </Router>
    
  );

 
}

export default App;
