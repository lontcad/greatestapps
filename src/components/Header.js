import React from "react";


function Header() {
  return (
    <div
    style={{
      justifyContent: "center",
      alignItems: "center",
      display: "grid",
      backgroundcolor: "#2196F3",
      padding: "5px"
    }}>
    <h1>Welcome to your Greatest App Ever</h1>
    <p>Your new single-page application to  to request titles from the page that a customer is looking for, built with:</p>
    <ul>
      <li><a href='https://spring.io/guides/gs/spring-boot/'>Spring Boot </a> and <a href='https://java.com'>Java 8</a> for platform server-side code</li>
      <li><a href='https://facebook.github.io/react/'>ReactJS</a>  for client-side code</li>
      <li><a href='http://getbootstrap.com/'>Bootstrap</a> for layout and styling</li>
    </ul>
    <p>To help you get started, we've also set up:</p>
    <ul>
      <li><strong>Client Side </strong> Text field where the end user should enter a valid URL without <strong>http://</strong> or <strong>https://</strong>. For example, www.azeti.net or azeti.net and click on  <em>Request Title</em> then the App will display the title of the request page.</li>
      <li><strong>API Dev server integration</strong> from <code>Spring Boot</code> should be up and running in the background, so your client-side resources could interact with Rest API dynamically to get the desired Data.</li>
    </ul>
   </div>
  );
}
export default Header;



      