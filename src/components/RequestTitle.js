import React, { Component } from 'react';
import {useEffect,useState} from 'react';
import styled from "@emotion/styled";
import axios from "axios"; 
import { toast } from 'react-toastify';
import '../css/ReactToastify.css';
import '../css/bootstrap.min.css';


toast.configure();


const Button = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid #0e3094;
  color: #0e3094;
  `;



class RequestTitle extends Component {
  
  constructor(props) {
    super(props);
    this.state = {pageadress: ''};
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   
  }


 handleSubmit = event => {

    event.preventDefault();   
    var valid = /^(http|https):\/\/[^ "]+$/.test(this.state.pageadress);

    if (valid)
    toast("Please remove the suffix http:// or https:// in the URL and submit again! ", {autoClose:10000,position:toast.POSITION.TOP_CENTER});
    
    else{
    
     if (this.state.pageadress == "")
     toast("URL field is empty please verify and submit again! ", {autoClose:10000,position:toast.POSITION.TOP_CENTER});
     else
     
       axios.get("http://localhost:8082/title/"+this.state.pageadress)
      .then(res=>{
        //console.log(res.data.ad);
         if (res.data == "")
         toast("No Data available. Please check the URL or Internet connection and submit again! ", {autoClose:10000,position:toast.POSITION.TOP_CENTER});
         else
         toast("The title of the submitted page is : " + res.data, {autoClose:10000,position:toast.POSITION.TOP_CENTER});
       
      })
  }
}


handleChange = event =>{
    this.setState({ pageadress: event.target.value});
}
render() {
    return (
       
        <form onSubmit = { this.handleSubmit }>
          
          <div class="input-group flex-nowrap">
            <span class="input-group-text" id="addon-wrapping">URL:</span>
            <input type="text" class="form-control" aria-describedby="addon-wrapping"  placeholder="Page Adress" aria-label="azeti.net" pageadress = "pageadress" onChange= {this.handleChange}/>
            <Button variant="primary" size="lg" type = "submit"> Request Title </Button>

          </div>

         
        </form>
    
    );
  }
}
export default RequestTitle;